using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class ClawSwipe : MonoBehaviour
    {
        //Reference claw swipe colliders
        [SerializeField] private Renderer[] rows;

        //number for random index
        public int index;

        //reference to active row
        Renderer rowToActivate;

        //Function for calling attack
        public void ClawSwipeAttack()
        {
            //Activate random row
            index = Random.Range(0, rows.Length);
            rowToActivate = rows[index];
            rowToActivate.enabled = true; 

            //Check for possible targets inside of active row
            foreach(GameObject player in AttackButton.players)
            {
                if (rowToActivate.bounds.Contains(player.transform.position))
                {
                    //Check if they get shielded and assign damage based on that 
                    if (player.GetComponent<AttackButton>().shieldActive == false)
                    {
                        //let attackable players take damage
                        player.GetComponent<PlayerHealth>().TakeDamage(20);

                        Debug.Log(player.name + " taking damage claw swipe");
                    }
                    else
                    {
                        GameObject.Find("Tank").gameObject.GetComponent<PlayerHealth>().TakeDamage(20);
                        player.GetComponent<AttackButton>().shieldActive = false;
                        Debug.Log(player.name + " tank takes damage");
                    }
                }

                
            }

            //Deactivate active row
            rowToActivate.enabled = false;
        }
    }
}
