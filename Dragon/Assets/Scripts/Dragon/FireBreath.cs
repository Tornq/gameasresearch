using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class FireBreath : MonoBehaviour
    {
        //reference to firebreath holder
        [SerializeField] private GameObject fireBreathColliderHolder;

        //refernce to different damage area renderers
        [SerializeField] private Renderer fireBreathColliderRed, fireBreathColliderOrange, fireBreathColliderYellow;

        private void Start()
        {
            //Deactivate  renderers in the beginning 
            fireBreathColliderHolder.SetActive(false); 
        }

        //Activates the indicator in first round
        public void ActivateAttack()
        {
            fireBreathColliderHolder.SetActive(true); 
        }

        //Call actual damage method after indicating 
        public void FireAttack()
        {
            Debug.Log("Fire attck damaging");

            //Check for each player if transform is in one of the firebrath renderers
            foreach (GameObject player in AttackButton.players)
            {
                
                if (fireBreathColliderRed.bounds.Contains(player.transform.position))
                    {
                        //Check if they get shielded and assign damage based on that 
                        if (player.GetComponent<AttackButton>().shieldActive == false)
                        {
                            player.GetComponent<PlayerHealth>().TakeDamage(35);
                            Debug.Log(player.name + " takes damage in collider red");
                        }
                        else
                        {
                            GameObject.Find("Tank").gameObject.GetComponent<PlayerHealth>().TakeDamage(35);
                            player.GetComponent<AttackButton>().shieldActive = false;
                            Debug.Log(player.name + " tank takes damage");
                        }
                    }

                if (fireBreathColliderOrange.bounds.Contains(player.transform.position))
                {
                    //Check if they get shielded and assign damage based on that 
                    if (player.GetComponent<AttackButton>().shieldActive == false)
                    {
                        player.GetComponent<PlayerHealth>().TakeDamage(20);
                        Debug.Log(player.name + " takes damage in collider orange");
                    }
                    else
                    {
                        GameObject.Find("Tank").gameObject.GetComponent<PlayerHealth>().TakeDamage(20);
                        player.GetComponent<AttackButton>().shieldActive = false;
                        Debug.Log(player.name + " tank takes damage");
                    }
                }

                if (fireBreathColliderYellow.bounds.Contains(player.transform.position))
                {
                    //Check if they get shielded and assign damage based on that 
                    if (player.GetComponent<AttackButton>().shieldActive == false)
                    {
                        player.GetComponent<PlayerHealth>().TakeDamage(10);
                        Debug.Log(player.name + " takes damage in collider yellow");
                    }
                    else
                    {
                        GameObject.Find("Tank").gameObject.GetComponent<PlayerHealth>().TakeDamage(10);
                        player.GetComponent<AttackButton>().shieldActive = false;
                        Debug.Log(player.name + " tank takes damage");
                    }
                }

                //End the attack 
                EndAttack(); 
            }
        }

        //Deactivate firebreath renderers
        public void EndAttack()
        {
            Debug.Log("End Fire Attack");

            fireBreathColliderHolder.SetActive(false);
        }
    }
}
