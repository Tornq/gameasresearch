using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class BiteAttack : MonoBehaviour
    {
        //Targets possible to attack
        public List<GameObject> targets = new List<GameObject>();
        public int randomIndex;
        //One target to attack
        public GameObject targetToAttack;
        public Renderer attackCollider;
        private bool biteStart;

        void Update()
        {
            //All units that are in range for bite get added to the list
            //If they are not in range or dead, we remove them
            foreach (GameObject player in AttackButton.players)
            {
                if (attackCollider.bounds.Contains(player.transform.position))
                {
                    gameObject.GetComponent<DragonManager>().inRangeForBite = true;

                    if (!targets.Contains(player))
                    {
                        targets.Add(player);
                    }
                }
                else
                {
                    targets.Remove(player);
                }

                if (player.GetComponent<PlayerHealth>().currenthealth <= 0)
                {
                    targets.Remove(player);
                }

                if (targets.Count == 0)
                {
                    gameObject.GetComponent<DragonManager>().inRangeForBite = false;
                }

            }
        }

        //Damages a random unit which is in range to be attacked
        public void Bite()
        {
            randomIndex = Random.Range(0, targets.Count);
            if (randomIndex > targets.Count || randomIndex < 0)
            {
                randomIndex = Random.Range(0, (targets.Count - 1));
            }
            else
            {

                targetToAttack = targets[randomIndex];
                if (targetToAttack.GetComponent<AttackButton>().shieldActive == false )
                {
                    if(targetToAttack.GetComponent<PlayerMove>().isAlive == true)
                    {
                        targetToAttack.GetComponent<PlayerHealth>().TakeDamage(25);
                        Debug.Log("Target:" + targetToAttack);
                    }

                }
                //If the tank is shielding, he takes the damage instead
                else
                {
                    if(targetToAttack.GetComponent<PlayerMove>().isAlive == true)
                    {
                        GameObject.Find("Tank").gameObject.GetComponent<PlayerHealth>().TakeDamage(25);
                        targetToAttack.GetComponent<AttackButton>().shieldActive = false;
                        Debug.Log("Tanks takes damage for" + targetToAttack.name);
                    }

                }
            }
        }
    }
}
