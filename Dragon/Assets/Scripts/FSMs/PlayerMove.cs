using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM; 

namespace PTWO_PR
{
    public class PlayerMove : TacticsMove
    {
        private StateMachine fsm; 

        public static bool checkingTiles = false;

        public  bool wasDamaged;

        [SerializeField] GameObject attackPanel, deathPanel;

        public bool isAlive = true;

        [SerializeField] private GameObject gameOverPanel;

        //References to audio; 
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip endScreenSound;

        void Start()
        {
            fsm = new StateMachine();

            //Add all states
            fsm.AddState("ALIVE", onLogic:OnLogicAlive);
            fsm.AddState("DEAD", onLogic:OnLogicDead);

            //Add transition
            fsm.AddTransition("ALIVE", "DEAD", FromAliveToDead); 

            //Set start state
            fsm.SetStartState("ALIVE");
            fsm.Init(); 

            Init();
        }

        void Update()
        {
            //Switch states when transistion gets called/activated
            fsm.OnLogic();

            //If we are checking for tiles and its the units turn, it moves
            if (checkingTiles == true)
            {
                if (!turn)
                {
                    return;
                }

                if (!moving)
                {
                    FindSelectableTiles();
                    CheckMouse();
                }
                else
                {
                    Move();                  
                }
            }

            //Remove tile indicat when no tiles should be found/active
            if(foundTile == false)
            {
                RemoveSelectableTiles();
            }

            //if both melee and ranged are dead the game is over
            if (GameObject.Find("Melee").GetComponent<PlayerMove>().isAlive == false && GameObject.Find("Ranged").GetComponent<PlayerMove>().isAlive == false)
            {
                audioSource.PlayOneShot(endScreenSound);
                gameOverPanel.SetActive(true);
            }
        }

        //Checks which tile was clicked to move to
        void CheckMouse()
        {
            if (Input.GetMouseButtonUp(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;

                if(Physics.Raycast(ray, out hit))
                {
                    if(hit.collider.tag == "Tile")
                    {
                        Tile t = hit.collider.GetComponent<Tile>();

                        if (t.selectable)
                        {
                            MoveToTile(t); 
                        }
                    }
                }
            }
        }

        //Wait button to wait a turn
        public void WaitButton()
        {
            //Failsafe remove tiles again when pressing wait button
            if(foundTile == true)
            {
                RemoveSelectableTiles();
            }

            //moving = false;
            TurnManager.EndTurn();
            Debug.Log("Waiting"); 
        }
        
        //Alive state
        private void OnLogicAlive(State<string> obj)
        {
            Debug.Log(gameObject.name + " is alive");

            //If its the units turn we activate the right panel
            if (turn)
            {
                attackPanel.SetActive(true);
                deathPanel.SetActive(false);
            }
        }

        //If the unit dies we enter the dead state
        private bool FromAliveToDead(Transition<string> arg)
        {
            if(gameObject.GetComponent<PlayerHealth>().currenthealth <= 0)
            {
                Debug.Log(gameObject.name + "entering dead state"); 
            }
            return gameObject.GetComponent<PlayerHealth>().currenthealth <= 0; 
        }

        //Dead state
        private void OnLogicDead(State<string> obj)
        {
            //if its the units turn we activate the right panel
            if (turn)
            {
                Debug.Log("DEAD");
                attackPanel.SetActive(false);
                deathPanel.SetActive(true);
            }
            //We remove the unit from the queue
            TurnManager.RemoveUnit(this);
        }
    }
}
