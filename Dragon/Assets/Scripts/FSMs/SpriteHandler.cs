using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM; 

namespace PTWO_PR
{

    public class SpriteHandler : MonoBehaviour
    {
        //Our state machine
        private StateMachine fsm;

        //units animator to switch animations
        private Animator playerAnim;

        //Number to set the sate of the FSM
        int number;

        //Reference to the dragon
        public GameObject dragon;

        private void Start()
        {
            //Get the animator of each unit
            playerAnim = GetComponentInChildren<Animator>(); 

            //Add all states to the FSM
            fsm = new StateMachine();
            fsm.AddState("IDLE", OnLogicIdle);
            fsm.AddState("WALK", OnLogicWalk);
            fsm.AddState("ABILITY", OnLogicAbility);
            fsm.AddState("DEAD", OnLogicDead);

            //Add all transitions to the fsm
            fsm.AddTransition("IDLE", "WALK", FromIdleToWalk);
            fsm.AddTransition("IDLE", "ABILITY", FromIdleToAbility);
            fsm.AddTransition("IDLE", "DEAD", FromIdleToDead);
            fsm.AddTransition("WALK", "IDLE", FromStateToIdle);
            fsm.AddTransition("ABILITY", "IDLE", FromStateToIdle);

            //Set the start state
            fsm.SetStartState("IDLE");

            //Initialize FSM
            fsm.Init(); 
        } 

        private void Update()
        {
            //On logic so the fsm can switch
            fsm.OnLogic();

            if (gameObject.GetComponent<PlayerMove>().moving == true)
            {
                //IDLE TO WALK
                number = 1;
            }
            if (gameObject.GetComponent<AttackButton>().attackActive == true)
            {
                //IDLE TO ABILITY
                number = 2;
            }
            if (gameObject.GetComponent<PlayerHealth>().currenthealth <= 0)
            {
                //IDLE TO DEAD
                number = 3;
            }

            if (gameObject.GetComponent<PlayerMove>().moving == false && gameObject.GetComponent<AttackButton>().attackActive == false && gameObject.GetComponent<PlayerHealth>().currenthealth > 0)
            {
                //IDLE
                number = 0;
            }
        }

        //Idle state
        private void OnLogicIdle(State<string> obj)
        {
            Debug.Log(gameObject.name + " is idling");
        }

        //Walk state
        private void OnLogicWalk(State<string> obj)
        {
            Debug.Log(gameObject.name + " is walking");
        }

        //Ability state
        private void OnLogicAbility(State<string> obj)
        {
            Debug.Log(gameObject.name + " is using ability");
        }

        //Dead state
        private void OnLogicDead(State<string> obj)
        {
            Debug.Log(gameObject.name + " is dead");
        }

        //Transition from Idle to Walk if the number got updated to 1
        private bool FromIdleToWalk(Transition<string> arg)
        {
           if (number == 1)
           {
                Debug.Log(gameObject.name + "entering WALK state");
                //Set the animator bool to play the walk anim
                playerAnim.SetBool("IsWalking", true);

            }
            return number == 1; 
        }

        //Transition from Idle to Ability if the number got updated to 2
        private bool FromIdleToAbility(Transition<string> arg)
        {
            if(number == 2)
            {
                Debug.Log(gameObject.name + "entering ABILITY state");
                //Set the animator bool to play the ability anim
                playerAnim.SetBool("UsingAbility", true); 
            }
            return number == 2; 
        }

        //Transition from Idle to Dead if the number got updated to 3
        private bool FromIdleToDead(Transition<string> arg)
        {
            if (number == 3)
            {
                Debug.Log(gameObject.name + "entering DEAD state");
                //Set the animator bool to play the dead anim
                playerAnim.SetBool("Dead", true); 
            }
            return number == 3;
        }

        //Transition from any state to Idle if the number got updated to 0
        private bool FromStateToIdle(Transition<string> arg)
        {
            if (number == 0)
            {
                Debug.Log(gameObject.name + "entering IDLE from STATE ");
                //Setting the animator bools to playing the idle anim
                playerAnim.SetBool("IsWalking", false);
                playerAnim.SetBool("UsingAbility", false);
            }
            return number == 0;
        }
    }
}
