using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM; 

namespace PTWO_PR
{
    public class DragonManager : TacticsMove
    {
        private StateMachine fsm; 

        //Bool preparing an attack
        private static bool preparing;
        //Bool preparing fire attack
        [SerializeField] private bool fireAttack;
        //Bool in range to attack
        public bool inRangeForBite;
        //bool if dragon has attacked
        private bool hasAttacked;
        //Collider if enemies are in range to be attacked
        [SerializeField] private GameObject inRangeCollider;
        //Rows for AOE attacks
        [SerializeField] private GameObject DCollider;
        //Number that corresponds to attack
        [SerializeField] public int attackNumber; 
        //All players
        public static GameObject[] players;
        //Wait timer
        float wait = 1;
        //Timer to play animations
        float attackTimer = 3;
        //Bools to determine which attack we are casting
        public bool damagingWithFire, swiping, biting;
        //Panel that is displayed when the game ends
        [SerializeField] private GameObject gameOverPanel;

        //References to audio; 
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip endScreenSound; 

        void Start()
        {
            fsm = new StateMachine();
            //Add all states
            fsm.AddState("ALIVE", OnLogicAlive);
            fsm.AddState("DEAD", OnLogicDead);
            //Add transition
            fsm.AddTransition("ALIVE", "DEAD", FromAliveToDead);
            //Set start state
            fsm.SetStartState("ALIVE");
            fsm.Init();

            //Add unit to queue
            Init();
            //Add all players to array
            players= GameObject.FindGameObjectsWithTag("Player");
        }

        void Update()
        {
            //Execute the FSM and check for transitions
            fsm.OnLogic(); 

            //If it isn't the dragon's turn
            if (!turn)
            {
                //Reset wait
                wait = 0.5f;
                //Reset Attack timer
                attackTimer = 3;
                //Dragon hasn't attacked
                hasAttacked = false;
                //Deactivate collider so units can move
                inRangeCollider.SetActive(false);
                //Set all bools to false
                damagingWithFire = false; swiping = false; biting = false;
            }
            //If its the dragons turn
            else
            {
                //Waiting to execute
                wait = wait - Time.deltaTime;
                //Activate the collider to see if dragon can reach enemies
                inRangeCollider.SetActive(true);
                if(wait <= 0)
                {
                    //If we arent casting fire
                    if (fireAttack == false)
                    {
                        //Random number to define wich attack will be used
                        attackNumber = Random.Range(1, 10);

                        if (attackNumber <= 3)
                        {
                            //If the dragon can reach and hasn't attacked
                            if (inRangeForBite == true && hasAttacked == false)
                            {
                                //Bite attack and has attacked to true so the dragon doesnt attack twice
                                gameObject.GetComponent<BiteAttack>().Bite();
                                hasAttacked = true;
                                biting = true;
                                Debug.Log("Bite attack");
                                
                            }
                            //if the dragon can't reach we use fire attack instead
                            else
                            {
                                if (hasAttacked == false && fireAttack == false)
                                {
                                    hasAttacked = true;
                                    Debug.Log("Fire attack");
                                    gameObject.GetComponent<FireBreath>().ActivateAttack(); 
                                    fireAttack = true;
                                    //Ends the turn instantly
                                    TurnManager.EndTurn();
                                }
                            }
                        }

                        //Claw attack of the dragon
                        if (attackNumber <= 7 && attackNumber > 4)
                        {
                            if(hasAttacked == false)
                            {
                                swiping = true;
                                Debug.Log("Claw attack");
                                if(hasAttacked == false)
                                {
                                    gameObject.GetComponent<ClawSwipe>().ClawSwipeAttack();
                                    hasAttacked = true;
                                }

                            }
                            
                        }

                        if (attackNumber <= 10 && attackNumber > 7)
                        {
                            //If the dragon hasn't attacked we cast fire
                            if (hasAttacked == false && fireAttack == false)
                            {
                                hasAttacked = true;
                                Debug.Log("Fire attack");
                                gameObject.GetComponent<FireBreath>().ActivateAttack();
                                fireAttack = true;
                                //Ends the turn instantly
                                TurnManager.EndTurn();
                            }
                        }
                    }
                    //if we are casting fire and haven't attacked yet we let the units take damage
                    if (fireAttack == true && hasAttacked == false)
                    {
                        hasAttacked = true;
                        gameObject.GetComponent<FireBreath>().FireAttack();
                        fireAttack = false;
                        damagingWithFire = true;
                        Debug.Log("DAmaging with fire");
                    }
                }                              
            }

            //If the hasattacked bool is true, we start our timer
            if(hasAttacked == true)
            {
                attackTimer = attackTimer -= Time.deltaTime;
            }
            //When the timer hits 0 we end the turn
            if (attackTimer <= 0)
            {
                TurnManager.EndTurn();
            }
        }

        //The dragon is alive
        private void OnLogicAlive(State<string> obj)
        {
            Debug.Log(gameObject.name + " is alive");
        }

        //Transition if the dragon dies
        private bool FromAliveToDead(Transition<string> arg)
        {
            //if the dragons healt is 0 we switch the state
            if (gameObject.GetComponent<PlayerHealth>().currenthealth <= 0)
            {
                Debug.Log(gameObject.name + "entering dead state");
            }
            return gameObject.GetComponent<PlayerHealth>().currenthealth <= 0;
        }

        //Dead state of the dragon
        private void OnLogicDead(State<string> obj)
        {
            //The game is over and the panel gets activated
            audioSource.PlayOneShot(endScreenSound);
            gameOverPanel.SetActive(true);
            Time.timeScale = 0f;
            Debug.Log("Dragon dead state");
        }
    }

}
