using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;

namespace PTWO_PR
{
    public class SaveLoadFSM : MonoBehaviour
    {
        //All GO that we want to save
        public GameObject meleeToSave, rangedToSave, tankToSave, healerToSave, dragonToSave;

        //A number that will define which state we need to enter
        [SerializeField] private int number;

        //Our state machine
        private StateMachine fsm;

        [SerializeField] private GameObject continueButton; 

        private void Start()
        {
            //Create the fsm
            fsm = new StateMachine();

            //Add all states
            fsm.AddState("PLAY", onLogic:OnLogicPlay);
            fsm.AddState("SAVING", onLogic:OnLogicSave);
            fsm.AddState("LOADING", OnEnterLoad);

            //Add all transitions
            fsm.AddTransition("PLAY", "SAVING", FromPlayToSave);
            fsm.AddTransition("PLAY", "LOADING", FromPlayToLoad);
            fsm.AddTransition("SAVING", "PLAY", FromStateToPlay);
            fsm.AddTransition("LOADING", "PLAY", FromStateToPlay);

            //Set start state and init the fsm
            fsm.SetStartState("PLAY");
            fsm.Init();
        }

        private void Update()
        {
            //On logic to update states
            fsm.OnLogic();

            //If its the dragon's turn we set the number to one that will trigger the save event
            if(dragonToSave.GetComponent<DragonManager>().turn == true)
            {
                number = 3;
            }
            //if it isnt the dragon's turn we set the number to one that will set the play/idle state
            else
            {
                number = 0;
            }

            //If there is no save we don't want to have a continue button
            if (meleeToSave == null && rangedToSave == null && tankToSave == null && healerToSave == null && dragonToSave == null)
            {
                continueButton.SetActive(false);
            }
            //If a save exists we activate a continue button
            else
            {
                continueButton.SetActive(true);
            }
        }

        //One we press the continue button we set the number to one that will trigger the load event
        public void Continue()
        {
            number = 1;
        }

        //On logic play to check if we are in the play/idle mode
        private void OnLogicPlay(State<string> obj)
        {
            Debug.Log("OY IM FOOKIN DOIN IT MATE");
        }

        //Transition if our number is higher than 2 into the save state
        private bool FromPlayToSave(Transition<string> arg)
        {
            if(number >= 2)
            {
                Debug.Log("SAVING");
            }
            return number >= 2;
        }
        //Transition if our number is smaller or equal than 1 but higher than 0 into the load state
        private bool FromPlayToLoad(Transition<string> arg)
        {
            if(number <= 1 && number > 0)
            {
                Debug.Log("LOADING");
            }
            return number <= 1 && number > 0;
        }
        //Transition from both save and loading back into the play/idle state
        private bool FromStateToPlay(Transition<string> arg)
        {
            if(number <= 0)
            {
                Debug.Log("PLAY");
            }
            return number <= 0;
        }

        //When we are in the save state we use playerprefs to save both the position (X/Y/Z) and health of each unit
        private void OnLogicSave(State<string> obj)
        {
            Debug.Log("AYE SAVING YOUR LIFE TODAY");
            //Setting values
            PlayerPrefs.SetFloat(meleeToSave.name + " PosX", meleeToSave.transform.position.x);
            PlayerPrefs.SetFloat(meleeToSave.name + " PosY", meleeToSave.transform.position.y);
            PlayerPrefs.SetFloat(meleeToSave.name + " PosZ", meleeToSave.transform.position.z);
            PlayerPrefs.SetFloat(meleeToSave.name + " currentHealth", meleeToSave.GetComponent<PlayerHealth>().currenthealth);

            PlayerPrefs.SetFloat(rangedToSave.name + " PosX", rangedToSave.transform.position.x);
            PlayerPrefs.SetFloat(rangedToSave.name + " PosY", rangedToSave.transform.position.y);
            PlayerPrefs.SetFloat(rangedToSave.name + " PosZ", rangedToSave.transform.position.z);
            PlayerPrefs.SetFloat(rangedToSave.name + " currentHealth", rangedToSave.GetComponent<PlayerHealth>().currenthealth);

            PlayerPrefs.SetFloat(tankToSave.name + " PosX", tankToSave.transform.position.x);
            PlayerPrefs.SetFloat(tankToSave.name + " PosY", tankToSave.transform.position.y);
            PlayerPrefs.SetFloat(tankToSave.name + " PosZ", tankToSave.transform.position.z);
            PlayerPrefs.SetFloat(tankToSave.name + " currentHealth", tankToSave.GetComponent<PlayerHealth>().currenthealth);

            PlayerPrefs.SetFloat(healerToSave.name + " PosX", healerToSave.transform.position.x);
            PlayerPrefs.SetFloat(healerToSave.name + " PosY", healerToSave.transform.position.y);
            PlayerPrefs.SetFloat(healerToSave.name + " PosZ", healerToSave.transform.position.z);
            PlayerPrefs.SetFloat(healerToSave.name + " currentHealth", healerToSave.GetComponent<PlayerHealth>().currenthealth);

            PlayerPrefs.SetFloat(dragonToSave.name + " PosX", dragonToSave.transform.position.x);
            PlayerPrefs.SetFloat(dragonToSave.name + " PosY", dragonToSave.transform.position.y);
            PlayerPrefs.SetFloat(dragonToSave.name + " PosZ", dragonToSave.transform.position.z);
            PlayerPrefs.SetFloat(dragonToSave.name + " currentHealth", dragonToSave.GetComponent<PlayerHealth>().currenthealth);

            //Actual saving
            PlayerPrefs.Save();
        }

        //When we enter load we get all the playerprefs and apply them to each unit
        private void OnEnterLoad(State<string> obj)
        {
            Debug.Log("IM A LITTLE CUNT");
            
            //If there is no save we dont change the health/position
            if (meleeToSave == null && rangedToSave == null && tankToSave == null && healerToSave == null && dragonToSave == null)
            {
                meleeToSave.transform.position = gameObject.transform.position;
                rangedToSave.transform.position = gameObject.transform.position;
                tankToSave.transform.position = gameObject.transform.position;
                healerToSave.transform.position = gameObject.transform.position;
                dragonToSave.transform.position = gameObject.transform.position;
                meleeToSave.GetComponent<PlayerHealth>().currenthealth = gameObject.GetComponent<PlayerHealth>().currenthealth;
                rangedToSave.GetComponent<PlayerHealth>().currenthealth = gameObject.GetComponent<PlayerHealth>().currenthealth;
                tankToSave.GetComponent<PlayerHealth>().currenthealth = gameObject.GetComponent<PlayerHealth>().currenthealth;
                healerToSave.GetComponent<PlayerHealth>().currenthealth = gameObject.GetComponent<PlayerHealth>().currenthealth;
                dragonToSave.GetComponent<PlayerHealth>().currenthealth = gameObject.GetComponent<PlayerHealth>().currenthealth;
            }

            //If there is a save we apply our playerprefs data to each unit
            if (PlayerPrefs.HasKey(meleeToSave.name + " PosX"))
            {
                meleeToSave.transform.position = new Vector3(
                PlayerPrefs.GetFloat(meleeToSave.name + " PosX"),
                PlayerPrefs.GetFloat(meleeToSave.name + " PosY"),
                PlayerPrefs.GetFloat(meleeToSave.name + " PosZ"));
                meleeToSave.GetComponent<PlayerHealth>().currenthealth = (int)PlayerPrefs.GetFloat(meleeToSave.name + " currentHealth");
            }

            if (PlayerPrefs.HasKey(rangedToSave.name + " PosX"))
            {
                rangedToSave.transform.position = new Vector3(
                PlayerPrefs.GetFloat(rangedToSave.name + " PosX"),
                PlayerPrefs.GetFloat(rangedToSave.name + " PosY"),
                PlayerPrefs.GetFloat(rangedToSave.name + " PosZ"));
                rangedToSave.GetComponent<PlayerHealth>().currenthealth = (int)PlayerPrefs.GetFloat(rangedToSave.name + " currentHealth");
            }
            if (PlayerPrefs.HasKey(tankToSave.name + " PosX"))
            {
                tankToSave.transform.position = new Vector3(
                PlayerPrefs.GetFloat(tankToSave.name + " PosX"),
                PlayerPrefs.GetFloat(tankToSave.name + " PosY"),
                PlayerPrefs.GetFloat(tankToSave.name + " PosZ"));
                tankToSave.GetComponent<PlayerHealth>().currenthealth = (int)PlayerPrefs.GetFloat(tankToSave.name + " currentHealth");
            }
            if (PlayerPrefs.HasKey(healerToSave.name + " PosX"))
            {
                healerToSave.transform.position = new Vector3(
                PlayerPrefs.GetFloat(healerToSave.name + " PosX"),
                PlayerPrefs.GetFloat(healerToSave.name + " PosY"),
                PlayerPrefs.GetFloat(healerToSave.name + " PosZ"));
                healerToSave.GetComponent<PlayerHealth>().currenthealth = (int)PlayerPrefs.GetFloat(healerToSave.name + " currentHealth");
            }
            if (PlayerPrefs.HasKey(dragonToSave.name + " PosX"))
            {
                dragonToSave.transform.position = new Vector3(
                PlayerPrefs.GetFloat(dragonToSave.name + " PosX"),
                PlayerPrefs.GetFloat(dragonToSave.name + " PosY"),
                PlayerPrefs.GetFloat(dragonToSave.name + " PosZ"));
                dragonToSave.GetComponent<PlayerHealth>().currenthealth = (int)PlayerPrefs.GetFloat(dragonToSave.name + " currentHealth");
            }
        }
    }
}
