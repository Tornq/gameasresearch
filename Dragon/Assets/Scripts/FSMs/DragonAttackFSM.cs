using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;

namespace PTWO_PR
{
    public class DragonAttackFSM : MonoBehaviour
    {
        [SerializeField] private DragonManager dragon;

        private StateMachine fsm;
        //Attack references
        [SerializeField] private GameObject[] meteors;
        [SerializeField] private ClawSwipe swipe;
        [SerializeField] private BiteAttack bite;
        private GameObject meteorToActivate;
        [SerializeField] private GameObject fireBreath;

        //Number that determines which state we are in
        [SerializeField]private int number;

        //References for audio
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip fireBreathSound;
        [SerializeField] private AudioClip meteorSound;
        [SerializeField] private AudioClip biteSound;

        // Start is called before the first frame update
        void Start()
        {
            //We set the number that determines our state to 0/IDLE
            number = 0;

            fsm = new StateMachine();

            //Adding all states
            fsm.AddState("IDLE", onLogic:OnLogicIdle);
            fsm.AddState("FIRE", OnEnterFire);
            fsm.AddState("METEOR", OnEnterMeteor);
            fsm.AddState("IMPACT", OnEnterImpact);

            //Adding all transitions
            fsm.AddTransition("IDLE", "FIRE", FromIdleToFire);
            fsm.AddTransition("IDLE", "METEOR", FromIdleToMeteor);
            fsm.AddTransition("IDLE", "IMPACT", FromIdleToImpact);
            fsm.AddTransition("METEOR", "IDLE", FromStateToIdle);
            fsm.AddTransition("FIRE", "IDLE", FromStateToIdle);
            fsm.AddTransition("IMPACT", "IDLE", FromStateToIdle);

            //Setting start state to idle
            fsm.SetStartState("IDLE");
            fsm.Init();
        
        }

        // Update is called once per frame
        void Update()
        {

            //Calling on logic so our states get updated
            fsm.OnLogic();

            //If its the dragons turn
            if(dragon.turn == true)
            {
                //If the dragon is using the fire attack, we enter the FIRE state with number 1
                if(dragon.damagingWithFire == true)
                {
                    number = 1;
                }
                //If the dragon is using the swipe/meteor attack we enter the METEOR state with number 2
                if(dragon.swiping == true)
                {
                    number = 2;
                }
                //If the dragon is using the bite/Impact attack we enter the IMPACT state with number 3
                if(dragon.biting == true)
                {
                    number = 3;
                }
            }
            //If it isnt the dragons turn we are in the idle state
            else
            {
                number = 0;
            }
        
        }

        //Function for camera shake on meteor 
        IEnumerator Shake(float duration, float magnitude)
        {
            //Reference to dragon cam
            GameObject cam = GameObject.FindGameObjectWithTag("DragonCam");

            //Set position of cam
            Vector3 orginalPos = cam.transform.localPosition;

            //Timer variable 
            float elapsed = 0.0f;

            //Check for time 
            while (elapsed < duration)
            {
                //Shake based on x and y value 
                float x = Random.Range(-1f, 1f) * magnitude;
                float y = Random.Range(-1f, 1f) * magnitude;

                cam.transform.localPosition = new Vector3(x, y, orginalPos.z);

                elapsed += Time.deltaTime;

                yield return null;
            }

            //reset time 
            cam.transform.localPosition = orginalPos;
        }

        //Transition from any of the states back to idle
        bool FromStateToIdle(Transition<string> arg)
        {
            if(number == 0)
            {
                Debug.Log("DRAGON is IDLE");
            }
            return number == 0;
        }

        //Transition from idle to fire
        bool FromIdleToFire(Transition<string> arg)
        {
            if(number == 1)
            {
                Debug.Log("DRAGON is FIRE");
            }
            return number == 1;
        }

        //Transition from Idle to Meteor
        bool FromIdleToMeteor(Transition<string> arg)
        {
            if(number == 2)
            {
                Debug.Log("DRAGON is METEOR");
            }
            return number == 2;
        }

        //Transition from Idle to Impact
        bool FromIdleToImpact(Transition<string> arg)
        {
            if(number == 3)
            {
                Debug.Log("IMPACT");
            }
            return number == 3;
        }

        //While we are in Idle, we deactivate all our effects 
        void OnLogicIdle(State<string> obj)
        {
            //Deactivates meteors
            foreach(GameObject m in meteors)
            {
                m.SetActive(false);
            }
            //Deactivates the fire
            fireBreath.SetActive(false);
            //Deactivates the bite
            if(bite.targetToAttack != null)
            {

                bite.targetToAttack.transform.GetChild(4).gameObject.SetActive(false);

            }
        }

        //We activate the meteor row that is currently dealing out damage
        void OnEnterMeteor(State<string> obj)
        {
            meteorToActivate = meteors[swipe.index];
            StartCoroutine(Shake(3f, .15f));
            audioSource.PlayOneShot(meteorSound);
            meteorToActivate.SetActive(true);
        }
        //We activate the fire effect
        void OnEnterFire(State<string> obj)
        {
            fireBreath.SetActive(true);
            audioSource.PlayOneShot(fireBreathSound); 
        }
        //We activate the impact effect of the unit thats getting damaged
        void OnEnterImpact(State<string> obj)
        {
            bite.targetToAttack.transform.GetChild(4).gameObject.SetActive(true);
            audioSource.PlayOneShot(biteSound);
        }
    }
}
