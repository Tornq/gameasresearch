using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class TacticsCamera : MonoBehaviour
    {
        //Middle of the tower
        public Transform target;
        [SerializeField] private Vector3 camOffset;
        [Range(0.01f, 1.0f)]
        public float smoothFactor = 0.5f;
        public bool lookAtTarget = false, rotateAroundTarget = true;
        public float rotationSpeed = 5.0f;
        
        private void LateUpdate()
        {
            //Rotating cam with Horizontal input around the middle of the tower
            if(rotateAroundTarget)
            {
                Quaternion camTurnAngle = Quaternion.AngleAxis(Input.GetAxis("Horizontal") * - rotationSpeed, Vector3.up);

                camOffset = camTurnAngle * camOffset;
            }

            Vector3 newPos = target.position + camOffset;
            transform.position = Vector3.Slerp(transform.position, newPos, smoothFactor);

            //If we are rotating, we look at the target
            if(rotateAroundTarget)
            {
                transform.LookAt(target);
            }
        }
    }
}
