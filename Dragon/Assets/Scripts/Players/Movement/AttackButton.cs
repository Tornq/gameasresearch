using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class AttackButton : MonoBehaviour
    {
        //Variables for detecting possible Melee and Ranged attacks
        [Header("Melee Attack")]
        [SerializeField] private Renderer damageAreaDragon;
        [SerializeField] private GameObject attackAreaMelee;
        public bool meleeAttackPossible = false;

        [Header("Ranged attack")]
        [SerializeField] private GameObject attackAreaRanged;
        public bool rangedAttackPossible = false;

        //Variables for detecting possible Tank attack
        [Header("Tank Attack")]
        [SerializeField] private Renderer attackAreaTank;
        [SerializeField] private GameObject shieldButtonPanel;
        [SerializeField] private GameObject unitsShieldButton;
        public bool canBeShielded;
        public bool shieldActive;
        public static GameObject[] players;

        //Variables for detecting possible Tank attack
        [Header("Healer Attack")]
        [SerializeField] private Renderer attackAreaHealer;
        [SerializeField] private GameObject healButtonPanel;
        [SerializeField] private GameObject unitsHealButton;
        public bool canBeHealed;
        private float timer = 2.5f;
        private bool startTimer = false; 

        public static bool checkingTiles = false;

        //Current unit
        [SerializeField] private GameObject healer, melee, ranged, tank;

        public static bool wasSet;

        //Ability buttons
        [SerializeField] private GameObject buttonHealer, buttonMelee, buttonRanged, buttonTank;
        private bool healerActive, meleeActive, rangedActive, tankActive;
        [SerializeField] private GameObject abilityPanel;

        private GameObject dragonEnemy;
        private PlayerHealth healthScriptDragon;

        public bool attackActive;
        
        //References to audio
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip meleeSound;
        [SerializeField] private AudioClip rangedSound;
        [SerializeField] private AudioClip tankSound;
        [SerializeField] private AudioClip healerSound;

        private void Start()
        {
            //finds the dragon and all players
            dragonEnemy = GameObject.Find("Dragon");
            healthScriptDragon = (PlayerHealth)dragonEnemy.GetComponent(typeof(PlayerHealth));
            players = GameObject.FindGameObjectsWithTag("Player");
        }

        private void Update()
        {
            //Check if Melee is in range for attacking dragon
            if (damageAreaDragon.bounds.Contains(attackAreaMelee.transform.position))
            {
                Debug.Log("Detecting possible melee attack");
                meleeAttackPossible = true;
            }
            else
            {
                meleeAttackPossible = false;
            }

            //Check if Ranged is in range for attacking dragon
            if (damageAreaDragon.bounds.Contains(attackAreaRanged.transform.position))
            {
                Debug.Log("Detecting possible Ranged attack");
                rangedAttackPossible = true;
            }
            else
            {
                rangedAttackPossible = false;
            }

            //Check if Players are within tank collider and set variable for each button activation
            foreach (GameObject player in players)
            {
                if (attackAreaTank.bounds.Contains(player.transform.position) && player.GetComponent<PlayerMove>().isAlive == true)
                {
                    player.GetComponent<AttackButton>().canBeShielded = true;
                    Debug.Log("Tank can shield" + player.name);
                }
                else
                {
                    player.GetComponent<AttackButton>().canBeShielded = false;
                    Debug.Log("Tank cannot shield " + player.name + " anymore");
                }
            }

            //Check if Players are within Healer collider and set variable for each button activation
            foreach (GameObject player in players)
            {
                if (attackAreaHealer.bounds.Contains(player.transform.position) && player.GetComponent<PlayerMove>().isAlive == true)
                {
                    player.GetComponent<AttackButton>().canBeHealed = true;
                    Debug.Log("Healer can heal" + player.name);
                }
                else
                {
                    player.GetComponent<AttackButton>().canBeHealed = false;
                    Debug.Log("Healer cannot heal" + player.name + " anymore");
                }
            }

            //Timer for ending Ability showcase 
            if (startTimer == true)
            {
                abilityPanel.SetActive(false);
                timer -= Time.deltaTime;

                if (timer <= 0)
                {
                    gameObject.transform.GetChild(3).gameObject.SetActive(false);
                    startTimer = false;
                    timer = 2.5f;
                    foreach (GameObject player in players)
                    {
                        player.GetComponent<AttackButton>().attackActive = false;
                    }
                    abilityPanel.SetActive(true);
                    TurnManager.EndTurn();
                }
            }

            //if its the healers turn, we deactivate the other ability buttons
            if (healer.activeSelf == true && wasSet == false)
            {
                buttonHealer.SetActive(true);
                buttonMelee.SetActive(false);
                buttonRanged.SetActive(false);
                buttonTank.SetActive(false);
                shieldButtonPanel.SetActive(false);

                wasSet = true;
            }

            //if its the melee turn, we deactivate the other ability buttons
            if (melee.activeSelf == true && wasSet == false)
            {
                buttonHealer.SetActive(false);
                buttonRanged.SetActive(false);
                buttonTank.SetActive(false);

                //if the meele is in range we activate her ability  button
                if (meleeAttackPossible == true)
                {
                    buttonMelee.SetActive(true);
                }
            }

            //if its the ranged turn, we deactivate the other ability buttons
            if (ranged.activeSelf == true && wasSet == false)
            {
                foreach(GameObject player in players)
                {
                    shieldActive = false;
                }

                GameObject.Find("Tank").gameObject.transform.GetChild(2).gameObject.SetActive(false);
                buttonMelee.SetActive(false);
                buttonHealer.SetActive(false);
                buttonTank.SetActive(false);

                canBeHealed = false;

                //if the ranged is in range we activate her ability  button
                if (rangedAttackPossible == true)
                {
                    buttonRanged.SetActive(true);
                }
            }

            //if its the tanks turn, we deactivate the other ability buttons
            if (tank.activeSelf == true && wasSet == false)
            {
                buttonTank.SetActive(true);
                buttonMelee.SetActive(false);
                buttonHealer.SetActive(false);
                buttonRanged.SetActive(false);
            }

            //If units are in range to be shielded, the corresponding button gets activates
            if (canBeShielded == true)
            {
                unitsShieldButton.SetActive(true);
            }
            if (canBeShielded == false)
            {
                unitsShieldButton.SetActive(false);
            }

            //If units are in range to be healed, the corresponding button gets activates
            if (healer.activeSelf == false)
            {
                healButtonPanel.SetActive(false);
            }
            if (canBeHealed == true)
            {
                unitsHealButton.SetActive(true);
            }
            if (canBeHealed == false)
            {
                unitsHealButton.SetActive(false);
            }
        }

        //Melee attack
        public void AttackButtonMelee()
        {
            attackActive = true;
            wasSet = true;
            healthScriptDragon.TakeDamage(25);
            buttonMelee.SetActive(false);
            audioSource.PlayOneShot(meleeSound);
            startTimer = true;
        }

        //Range attack
        public void AttackButtonRanged()
        {
            attackActive = true;
            wasSet = true;
            healthScriptDragon.TakeDamage(15);
            buttonRanged.SetActive(false);
            audioSource.PlayOneShot(rangedSound);
            startTimer = true;
        }

        //Healer ability
        public void AttackButtonHealer()
        {
            wasSet = true;
            Debug.Log("Healer heals");
            healButtonPanel.SetActive(true);
        }

        //Tank ability
        public void AttackButtonTank()
        {
            wasSet = true;
            Debug.Log("Tank protects");
            shieldButtonPanel.SetActive(true);
        }

        //Actual protecting of the chosen unit
        public void ProtectUnit()
        {
            GameObject.Find("Tank").GetComponent<AttackButton>().attackActive = true;

            audioSource.PlayOneShot(tankSound);

            if (this.gameObject.name == "Healer")
            {
                Debug.Log("Shield Healer");
            }
            if (this.gameObject.name == "Ranged")
            {
                Debug.Log("Shield Ranged");
            }

            if (this.gameObject.name == "Melee")
            {
                Debug.Log("Shield Melee");
            }
            buttonTank.SetActive(false);
            shieldButtonPanel.SetActive(false);
            shieldActive = true;
            GameObject.Find("Tank").gameObject.transform.GetChild(2).gameObject.SetActive(true);
            startTimer = true;
        }

        //Actual healing of the chosen unit
        public void HealUnit()
        {
            GameObject.Find("Healer").GetComponent<AttackButton>().attackActive = true;

            audioSource.PlayOneShot(healerSound);

            if (this.gameObject.name == "Tank")
            {
                Debug.Log("Heal Tank");
                gameObject.GetComponent<PlayerHealth>().AddHP(10);
                
            }
            if (this.gameObject.name == "Ranged")
            {
                Debug.Log("Heal Ranged");
                gameObject.GetComponent<PlayerHealth>().AddHP(10);
            }
            if (this.gameObject.name == "Melee")
            {
                Debug.Log("Heal Melee");
                gameObject.GetComponent<PlayerHealth>().AddHP(10);
            }
            buttonHealer.SetActive(false);
            healButtonPanel.SetActive(false);
            gameObject.transform.GetChild(3).gameObject.SetActive(true);
      
            startTimer = true; 
        }

    }
}
