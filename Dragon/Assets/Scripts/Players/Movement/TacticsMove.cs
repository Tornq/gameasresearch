using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class TacticsMove : MonoBehaviour
    {
        public GameObject buttonPanel;
        public GameObject currentUnit; 

        public bool turn = false; 

        //List of selectable tiles for clearing purposes
        List<Tile> selectableTiles = new List<Tile>();
        GameObject[] tiles;

        //All tiles and current tile
        Stack<Tile> path = new Stack<Tile>();
        Tile currentTile;

        //Bool to check if tiles are active
        public bool foundTile;

        //movement
        public bool moving = false; 
        public int move = 5;
        public float jumpHeight = 2;
        public float moveSpeed = 2; 

        Vector3 velocity = new Vector3();
        Vector3 heading = new Vector3();

        float halfHeight = 0;

        //cam anim
        private CinemachineSwitcher switcher;

        //Gets all tiles and adds the unit this script is attached to to the queue
        protected void Init()
        {
            tiles = GameObject.FindGameObjectsWithTag("Tile");

            halfHeight = GetComponent<Collider>().bounds.extents.y;

            switcher = GameObject.FindGameObjectWithTag("CamManage").GetComponent<CinemachineSwitcher>();

            TurnManager.AddUnit(this); 
        }

        //Gets the current tile the unit is standing on
        public void GetCurrentTile()
        {
            currentTile = GetTargetTile(gameObject);
            currentTile.current = true; 
        }

        //Gets the tile that is selected for movement
        public Tile GetTargetTile(GameObject target)
        {
            RaycastHit hit;
            Tile tile = null; 

            if(Physics.Raycast(target.transform.position, -Vector3.up, out hit, 1))
            {
                tile = hit.collider.GetComponent<Tile>(); 
            }

            return tile; 
        }

        //Finds all neighbouring tiles
        public void ComputeAdjacencyLists()
        {
            foreach (GameObject tile in tiles)
            {
                Tile t = tile.GetComponent<Tile>();
                t.FindNeighbors(jumpHeight); 
            }
        }

        //Finds all tiles that the unit can move to 
        public void FindSelectableTiles()
        {
            //Bool found tiles set to true when activating indicat
            foundTile = true;

            ComputeAdjacencyLists();
            GetCurrentTile();

            Queue<Tile> process = new Queue<Tile>();

            process.Enqueue(currentTile);
            currentTile.visited = true; 
            //currentTile.parent = ?? leave as null

            while (process.Count > 0)
            {
                Tile t = process.Dequeue();

                selectableTiles.Add(t);
                t.selectable = true; 

                //If the tile is not out of the units movement range
                if(t.distance < move)
                {
                    foreach (Tile tile in t.adjacencyList)
                    {
                        if (!tile.visited)
                        {
                            tile.parent = t;
                            tile.visited = true; 
                            tile.distance = 1 + t.distance;
                            process.Enqueue(tile);
                        }
                    }
                }
            }            
        }

        //Moves to the target tile tile
        public void MoveToTile(Tile tile)
        {
            path.Clear(); 
            tile.target = true;
            moving = true;
            Tile next = tile; 
            while(next != null)
            {
                path.Push(next); 
                next = next.parent; 
            }
        }

        //General movement of the unit
        public void Move()
        {
            if(path.Count > 0)
            {
                Tile t = path.Peek();
                Vector3 target = t.transform.position;

                //Calculate the unit�s position on top of the target tile
                target.y += halfHeight + t.GetComponent<Collider>().bounds.extents.y;

                if (Vector3.Distance(transform.position, target)>= 0.05f)
                {
                    CalculateHeading(target);
                    SetHorizontalVelocity();
                    transform.position += velocity * Time.deltaTime; 
                }
                else
                {
                    //Tile center reached
                    transform.position = target;
                    path.Pop(); 
                }
            }
            else
            {
                //Locomotion
                RemoveSelectableTiles();  
                moving = false;
                TurnManager.EndTurn(); 
            }
        }

        //Removes the tiles once we finished moving as we don't need to move again this turn
        protected void RemoveSelectableTiles()
        {
            //Bool found tiles set false when removing tile indicat
            foundTile = false;

            if(currentTile != null)
            {
                currentTile.current = false;
                currentTile = null; 
            }

            //Resets the tiles as we now changed positon
            foreach (Tile tile in selectableTiles)
            {
                tile.Reset(); 
            }

            selectableTiles.Clear(); 
        }

        //Pathfinding to the selected tile
        void CalculateHeading(Vector3 target)
        {
            heading = target - transform.position;
            heading.Normalize(); 
        }

        //Sets the speed of movement
        void SetHorizontalVelocity()
        {
            velocity = heading * moveSpeed; 
        }

        //Begins the turn and activates the ability panel
        public void BeginTurn()
        {
            turn = true;
            currentUnit.SetActive(true); 

            buttonPanel.SetActive(true);
            Debug.Log("Start turn " +this.gameObject.name);
        }

        //Ends the turn and resets all variables
        public void EndTurn()
        {
            //Bool found tiles set false at end of turn
            foundTile = false;

            turn = false;
            currentUnit.SetActive(false);

            PlayerMove.checkingTiles = false; 
            buttonPanel.SetActive(false);
            switcher.wasSet = false;
            AttackButton.wasSet = false; 
            Debug.Log("End turn");
            WalkButton.buttonPressed = false; 
        }
    }
}
