using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class TurnManager : MonoBehaviour
    {
        //List declaring every member/Unit of the team
        static Dictionary<string, List<TacticsMove>> units = new Dictionary<string, List<TacticsMove>>();

        //Key for Team that is currently active
        static Queue<string> turnKey = new Queue<string>();

        //Currently active/inactive team member(s)
        static Queue<TacticsMove> turnTeam = new Queue<TacticsMove>();

        void Update()
        {
            //If the queue is empty we initialize a queue
            if (turnTeam.Count == 0)
            {
                InitTeamTurnQueue();
            }
        }

        //If a turn is supposed to start but the queue is empty we initialize a queue
        public void InitTurn()
        {
            if (turnTeam.Count == 0)
            {
                InitTeamTurnQueue();
            }
        }

        //Enqueues the team and starts the turn
        static void InitTeamTurnQueue()
        {
            List<TacticsMove> teamList = units[turnKey.Peek()];  

            foreach (TacticsMove unit in teamList)
            {
                turnTeam.Enqueue(unit); 
            }

            StartTurn(); 
        }

        //If the queue is filled we start the turn
        public static void StartTurn()
        {
            if (turnTeam.Count > 0)
            {
                turnTeam.Peek().BeginTurn();
            }
        }

        //We dequeue the unit at the end of the turn
        //If the queue is still full we go to the next unit, if not we enqueue the units again
        public static void EndTurn()
        {
            TacticsMove unit = turnTeam.Dequeue();
            unit.EndTurn();
            
            if (turnTeam.Count > 0)
            {
                StartTurn(); 
            }
            else
            {
                string team = turnKey.Dequeue();
                turnKey.Enqueue(team);
                InitTeamTurnQueue(); 
            }
        }

        //Adds all tactic move units to the list of units and checks if they have been added yet
        public static void AddUnit(TacticsMove unit)
        {
            List<TacticsMove> list;

            if (!units.ContainsKey(unit.tag))
            {
                list = new List<TacticsMove>();
                units[unit.tag] = list;

                if (!turnKey.Contains(unit.tag))
                {
                    turnKey.Enqueue(unit.tag); 
                }
            }
            else
            {
                list = units[unit.tag]; 
            }

            list.Add(unit); 
        }

        //Removes a unit from the team
        public static void RemoveUnit(TacticsMove unit)
        {
            List<TacticsMove> list;
            list = units[unit.tag];

            list.Remove(unit);
        }
    }


}
