using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class Tile : MonoBehaviour
    {
        //Public variables for status of the tiles
        public bool walkable = true; 
        public bool current = false;
        public bool target = false;
        public bool selectable = false;

        //List of tiles for identification of neighbors
        public List<Tile> adjacencyList = new List<Tile>();

        //Needed BFS (breadth first search) 
        public bool visited = false; 
        public Tile parent = null;
        public int distance = 0; 

        void Update()
        {
            //Change tile color based on status
            if (current)
            {
                GetComponent<Renderer>().material.SetColor("Color_575b879203df4def912adbe64fcbeb04", Color.red);
            }
            else if (target)
            {
                GetComponent<Renderer>().material.SetColor("Color_575b879203df4def912adbe64fcbeb04", Color.green);
            }
            else if (selectable)
            {
                GetComponent<Renderer>().material.SetColor("Color_575b879203df4def912adbe64fcbeb04", Color.blue);
            }
            else
            {
                GetComponent<Renderer>().material.SetColor("Color_575b879203df4def912adbe64fcbeb04", Color.black);
            }
        }

        //Set tile back to default
        public void Reset()
        {
            adjacencyList.Clear(); 

            current = false;
            target = false;
            selectable = false;

            visited = false; 
            parent = null;
            distance = 0;
        }

        //Find neighbor tiles of the tile
        public void FindNeighbors(float jumpHeight)
        {
            //Set tiles back to the orignal state when function is called
            Reset();

            //Check tiles to the left, right, up and down direction
            CheckTile(Vector3.forward, jumpHeight);
            CheckTile(-Vector3.forward, jumpHeight);
            CheckTile(Vector3.right, jumpHeight);
            CheckTile(-Vector3.right, jumpHeight);
        }

        //Check each individual tile for a neighbor
        public void CheckTile(Vector3 direction, float jumpHeight)
        {
            //Find possible tiles based on colliders
            Vector3 halfExtents = new Vector3(0.25f, (1 + jumpHeight) / 2.0f, 0.25f); 
            Collider[] colliders = Physics.OverlapBox(transform.position + direction, halfExtents);

            //Check if tile should be added to adjacencyList
            foreach (Collider item in colliders)
            {
                Tile tile = item.GetComponent<Tile>(); 

                if(tile != null && tile.walkable)
                {
                    RaycastHit hit; 

                    if(!Physics.Raycast(tile.transform.position, Vector3.up, out hit, 1))
                    {
                        adjacencyList.Add(tile);
                    }

                }
            }
        }
    }
}
