using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class WalkButton : MonoBehaviour
    {
        public static bool buttonPressed;

        [SerializeField] private Animator camAnim;

        //If we press the walk button, we set the camera to the walk cam
        public void WalkButtonClick()
        {
            camAnim.Play("Move");
            PlayerMove.checkingTiles = true;
            
            buttonPressed = true;

        }
    }
}
