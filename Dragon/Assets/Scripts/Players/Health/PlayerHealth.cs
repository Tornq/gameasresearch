using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro; 

namespace PTWO_PR
{
    public class PlayerHealth : MonoBehaviour
    {
        //Health variables of player
        public int maxHealth = 100;
        public int currenthealth;

        //Variable to store the last updated health
        private int variable;

        //refernce to units healthbar and its text
        public Healthbar healthbar;
        [SerializeField] private TextMeshProUGUI textBox;

        private void Start()
        {
            //Sets the current health to the max health at the beginning
            currenthealth = maxHealth;
            variable = currenthealth;
            healthbar.SetMaxHealth(maxHealth);
            textBox.text = currenthealth.ToString() + " / " + maxHealth.ToString();
        }

        private void Update()
        {
            //Once the health changes we update the healthbar and text
            if(variable != currenthealth)
            {
                variable = currenthealth;
                healthbar.SetHealth(currenthealth);
                textBox.text = currenthealth.ToString() + " / " + maxHealth.ToString();

                if(currenthealth <= 0)
                {
                    textBox.text = "0 / " + maxHealth.ToString();
                }
            }
        }

        //Function to take damage
        public void TakeDamage(int damage)
        {
            //reduce health
            currenthealth -= damage;

            //Update healthbar
            healthbar.SetHealth(currenthealth);

            //If the health is 0, it can't go lower and the unit is dead
            if (currenthealth <= 0)
            {
                textBox.text = "0 / " + maxHealth.ToString();
                gameObject.GetComponent<PlayerMove>().isAlive = false; 
            }
            //if the unit is still alive we update the number
            else
            {
                textBox.text = currenthealth.ToString() + " / " + maxHealth.ToString();
            }
        }

        //Healing function
        public void AddHP(int hp)
        {
            //If we are not over max health, we add HP
            if (currenthealth < maxHealth)
            {
                currenthealth += hp;

                //If we would heal over max, we set it to max health instead
                if(currenthealth > maxHealth)
                {
                    currenthealth = maxHealth; 
                }

                //Update healthbar
                healthbar.SetHealth(currenthealth);
            }

            //If health is 0 or lower, we can not go higher and the unit is dead
            if (currenthealth <= 0)
            {
                textBox.text = "0 / " + maxHealth.ToString();
            }
            //if the unit is still alive we update the health number 
            else
            {
                textBox.text = currenthealth.ToString() + " / " + maxHealth.ToString();
            }
        }
    }
}
