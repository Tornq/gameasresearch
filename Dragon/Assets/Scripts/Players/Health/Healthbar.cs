using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

namespace PTWO_PR
{
    public class Healthbar : MonoBehaviour
    {
        //Slider that shows the health
        public Slider slider;
        //Gradient from greem to red to show how much health is left
        public Gradient gradient;
        //the fill of the health bar
        public Image fill; 

        //Sets the health of wach unit to the healthbar and updates it
        public void SetHealth(int health)
        {
            slider.value = health;

            fill.color = gradient.Evaluate(slider.normalizedValue); 
        }

        //Sets the healthbar to full health for each unit
        public void SetMaxHealth(int health)
        {
            slider.maxValue = health;
            slider.value = health;

            fill.color = gradient.Evaluate(1f); 
        }
    }
}
