using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class Billboard : MonoBehaviour
    {
        //refernce to camera transform
        public Transform cam;

        private void LateUpdate()
        {
            //Turns all sprtites towards the camera
            transform.LookAt(transform.position + cam.forward);
        }
    }
}
