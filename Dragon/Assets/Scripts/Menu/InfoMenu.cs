using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class InfoMenu : MonoBehaviour
    {
        [SerializeField] private GameObject infoPanel;
        //GO for the active unit
        [SerializeField] private GameObject healer, melee, ranged, tank;
        //Panel that corresponds to the active unit
        [SerializeField] private GameObject healerPanel, meleePanel, rangedPanel, tankPanel;
        

        void Update()
        {
            //Replaces the info according to the active unit
            if(healer.activeSelf == true)
            {
                healerPanel.SetActive(true);
                meleePanel.SetActive(false);
                rangedPanel.SetActive(false);
                tankPanel.SetActive(false);
            }
            if(melee.activeSelf == true)
            {
                healerPanel.SetActive(false);
                meleePanel.SetActive(true);
                rangedPanel.SetActive(false);
                tankPanel.SetActive(false);
            }
            if(ranged.activeSelf == true)
            {
                healerPanel.SetActive(false);
                meleePanel.SetActive(false);
                rangedPanel.SetActive(true);
                tankPanel.SetActive(false);
            }
            if(tank.activeSelf == true)
            {
                healerPanel.SetActive(false);
                meleePanel.SetActive(false);
                rangedPanel.SetActive(false);
                tankPanel.SetActive(true);
            }
        
        }

        //Opens and closes the menu depending on if its open or not
        public void OpenCloseInfo()
        {
            if(infoPanel.activeSelf == false)
            {
                infoPanel.SetActive(true);
            }
            else
            {
                infoPanel.SetActive(false);
            }
        }
    }
}
