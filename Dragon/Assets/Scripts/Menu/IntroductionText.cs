using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 

namespace PTWO_PR
{
    public class IntroductionText : MonoBehaviour
    {
        //Area for text 
        [TextArea(3, 10)]
        [SerializeField] private string text;

        //references
        [SerializeField] private TextMeshProUGUI introText;
        [SerializeField] private GameObject introPanel; 

        //Method to call from start button 
        public void StartIntro()
        {
            //Activate intro
            StartCoroutine(TypeIntro());
            introPanel.SetActive(true);  
        }

        //text writing effect
        IEnumerator TypeIntro()
        {          
            foreach(char letter in text.ToCharArray())
            {
                introText.text += letter;
                yield return new WaitForSeconds(0);
            }
        }
    }
}
